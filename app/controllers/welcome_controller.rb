class WelcomeController < ApplicationController
  require 'socket'

  def index
    if get_url_params
      if (@link = Link.where(url: slice_url(get_url_params)).take)
      else
         @link = Link.create(
            url: slice_url(get_url_params),
            generate_url: generate_url
        )
      end
    end
  end

  def redirect
    if (link = Link.where(generate_url: params[:generate_url]).take)
      redirect_to 'http://' + link[:url]
    else
      redirect_to root_path, flash: {errors: 'Please generate url'}
    end
  end

  def generate_url
    # render text: url.unpack('U*')
      if (generate_url = Link.last(1).empty? ? false :  Link.last[:generate_url])
        generate_url = generate_url.succ
      else
        generate_url = 'A0'
      end

    generate_url
  end

  def slice_url(url)

    if url.include? 'http://'
      url = url.gsub(/http:\/\//, '')
    elsif url.include? 'https://'
      url = url.gsub(/https:\/\//, '')
    end

    url
  end

  def get_url_params
    params[:url];
  end


end
